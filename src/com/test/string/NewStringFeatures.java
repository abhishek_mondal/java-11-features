package com.test.string;

import java.util.stream.Collectors;

public class NewStringFeatures {
    public static void main(String[] args) {
        //------isBlank method start-----------
        String str1 = "";
        System.out.println(str1.isBlank());
        String str2 = "Abhishek";
        System.out.println(str2.isBlank());
        //------isBlank method end-----------

        //------lines method start-----------
        String str = "Hello\nJava\n11\nworld";
        System.out.println(str.lines().collect(Collectors.toList()));
        //------lines method end-----------

        //------repeat method start-----------
        String str3 = "Abhishek";
        System.out.println(str3.repeat(4));
        //------repeat method end-----------

        //------stripLeading method start-----------
        String str4 = " Abhishek";
        System.out.println(str4.stripLeading());
        //------stripLeading method end-----------

        //-------stripTrailing method start-----------
        String str5 = "Abhishek ";
        System.out.println(str5.stripTrailing());
        //-------stripTrailing method end-----------

        //---------strip method start-----------
        String str6 = " Abhishek ";
        System.out.println(str6.strip());
        //---------strip method end-----------
    }
}
