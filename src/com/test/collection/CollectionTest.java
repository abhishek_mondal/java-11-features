package com.test.collection;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CollectionTest {
    public static void main(String[] args) {
        //toArray method
        List sampleList = Arrays.asList("Java", "Kotlin");
        String[] sampleArray = (String[]) sampleList.toArray(String[]::new);
        for(String str: sampleArray) {
            System.out.println(str);
        }

        //not predicate method
        List<String> sampleList1 = Arrays.asList("Java", "\n \n", "Kotlin", " ");
        List withoutBlanks = sampleList1.stream()
                .filter(Predicate.not(String::isBlank))
                .collect(Collectors.toList());
        System.out.println(withoutBlanks);

    }
}
